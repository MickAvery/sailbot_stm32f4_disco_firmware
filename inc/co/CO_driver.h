/* Copyright 2017 UBC Sailbot */

#ifndef _CO_DRIVER_H
#define _CO_DRIVER_H

#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>
#include "hal.h"

/* Critical sections */
#define CO_LOCK_EMCY()          chSysLock();
#define CO_UNLOCK_EMCY()        chSysUnlock();

#define CO_LOCK_OD()            chSysLock();
#define CO_UNLOCK_OD()          chSysUnlock();

/**
 * Macros to mask interrupts, used to protect critical sections.
 *
 * It is used in some places in library to protect short sections of code in
 * functions, which may be accessed from different tasks. Alternative solutions
 * are possible, for example disable only timer interrupt, if there is only
 * mainline and timer task.
 */
#define DISABLE_INTERRUPTS()  chSysLock();   /* Disable all interrupts */
#define ENABLE_INTERRUPTS()   chSysUnlock(); /* Reenable interrupts */

/**
 * Define endianness of MCU.
 * Comment out if MCU has big-endian byte order.
 */
#define CO_LITTLE_ENDIAN

/**
 * Debug flag.
 * Uncomment this macro if you're doing CO driver integration testing. Otherwise,
 * this should be commented out in the production code.
 */
#ifndef DRIVER_TEST
#define DRIVER_TEST
#endif

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief Error constants, return values of most CANopen functions
 */
typedef enum {
   CO_ERROR_NO                = 0,    /*<! Operation completed successfully */
   CO_ERROR_ILLEGAL_ARGUMENT  = -1,   /*<! Error in function arguments */
   CO_ERROR_OUT_OF_MEMORY     = -2,   /*<! Memory allocation failed */
   CO_ERROR_TIMEOUT           = -3,   /*<! Function timeout */
   CO_ERROR_ILLEGAL_BAUDRATE  = -4,   /*<! Illegal baudrate passed to function <CO_CANmodule_init> */
   CO_ERROR_RX_OVERFLOW       = -5,   /*<! Previous message was not processed yet */
   CO_ERROR_RX_PDO_OWERFLOW   = -6,   /*<! Previous PDO was not processed yet */
   CO_ERROR_RX_MSG_LENGTH     = -7,   /*<! Wrong receive message length */
   CO_ERROR_RX_PDO_LENGTH     = -8,   /*<! Wrong receive PDO length */
   CO_ERROR_TX_OVERFLOW       = -9,   /*<! Previous message is still waiting, buffer full */
   CO_ERROR_TX_PDO_WINDOW     = -10,  /*<! Synchronous TPDO is outside window */
   CO_ERROR_TX_UNCONFIGURED   = -11,  /*<! Transmit buffer was not confugured properly */
   CO_ERROR_PARAMETERS        = -12,  /*<! Error in function function parameters */
   CO_ERROR_DATA_CORRUPT      = -13,  /*<! Stored data are corrupt */
   CO_ERROR_CRC               = -14   /*<! CRC does not match */
} CO_ReturnError_t;

typedef bool bool_t;

/**
 * CAN receive message structure as aligned in CAN module. Object is passed to
 * <Receive CAN message function>. In general, that function only uses _DLC_ and
 * _data_ parameters. If it needs to read identifier, it must use function
 * <CO_CANrxMsg_readIdent>.
 */
typedef struct {
  uint32_t ident;   /*<! Standard Identifier */
  uint32_t ExtId;   /*<! Specifies the extended identifier */
  uint8_t  IDE;     /*<! Specifies the type of identifier for the message that will be received */
  uint8_t  RTR;     /*<! Remote Transmission Request bit */
  uint8_t  DLC;     /*<! Data length code (bits 0...3) */
  uint8_t  data[8]; /*<! 8 data bytes */
  uint8_t  FMI;     /*<! Specifies the index of the filter the message stored in the mailbox passes through */
  uint16_t TIME;    /*<!  */
} CO_CANrxMsg_t;


/**
 * \brief array for handling received CAN messages
 */
typedef struct{
    uint16_t    ident;  /*<! standard identifier (bits 0..10) + RTR (bit 11) */
    uint8_t     rtr;    /*<! remote transmission request bit */
    uint16_t    mask;   /*<! standard identifier mask with same alignment as ident */
    void       *object; /*<! see parameters in <CO_CANrxBufferInit> */
    void      (*pFunct)(void *object, const CO_CANrxMsg_t *message); /*<! see parameters in <CO_CANrxBufferInit> */
} CO_CANrx_t;

/**
 * \brief Array for handling transmitting CAN messages
 */
typedef struct {
  uint32_t        ident;      /*<! standard identifier (bits 0..10) + RTR (bit 11) */
  uint8_t         DLC;        /*<! Data length code (bits 0...3) */
  uint8_t         RTR;        /*<! Remote Transmission Request bit */
  uint8_t         data[8];    /*<! 8 data bytes, aligned to registers in transmit message buffer */
  uint8_t         bufferFull; /*<! if flag is set, previous message is still in buffer */
  uint8_t         syncFlag;   /*<! SYNC PDOs have this flag set, it prevents them to be sent outside the synchronous window */
} CO_CANtx_t;

/**
 * \brief CANopen module
 */
typedef struct{
  CAN_TypeDef        *CANbaseAddress;     /*<! CAN object base address, ChibiOS provides this as CAND1->can */
  CANDriver          *CANdrv;             /*<! CAN driver base address, either CAND1 or CAND2 */
  CO_CANrx_t         *rxArray;            /*<! Array for handling received CAN messages */
  uint16_t            rxSize;             /*<! Size of rxArray */
  CO_CANtx_t         *txArray;            /*<! Array for handling transmitting CAN messages */
  uint16_t            txSize;             /*<! Size of txArray */
  volatile bool_t     CANnormal;          /*<! CAN module is in normal mode. */
  volatile uint8_t    useCANrxFilters;    /*<! Value different than zero indicates, that CAN module hardware filters
                                               are used for CAN reception. If there is not enough hardware filters,
                                               they won't be used. In this case will be *all* received CAN messages
                                               processed by software. */
  volatile uint8_t    bufferInhibitFlag;  /*<! If flag is true, then message in transmitt buffer is synchronous PDO
                                               message, which will be aborted, if CO_clearPendingSyncPDOs() function
                                               will be called by application. This may be necessary if Synchronous
                                               window time was expired. */
  volatile uint8_t    firstCANtxMessage;  /*<! Is equal to 1, when the first transmitted message (bootup message) is in CAN TX buffers */
  volatile uint16_t   CANtxCount;         /*<! Number of messages in transmit buffer, which are waiting to be copied to the CAN module */
  uint32_t            errOld;             /*<! Previous state of CAN errors */
  void               *em;                 /*<! Emergency object */
} CO_CANmodule_t;

/********************************************************
 *************** Functions open to API ******************
 ********************************************************/

/**
 * \brief requests CAN configuration (stopped) mode and *wait* untill it is set
 *
 * \param[in] CANbaseAddress - CAN module base address, usually provided by RTOS HAL
 */
void CO_CANsetConfigurationMode(int32_t CANbaseAddress);

/**
 * \brief requests CAN normal (opearational) mode and *wait* untill it is set
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 */
void CO_CANsetNormalMode(CO_CANmodule_t *can);

/**
 * Initialize CAN module object.

 * Function must be called in the communication reset section. CAN module must
 * be in Configuration Mode before.
 * PIC32MX CAN FIFO configuration: Two FIFOs are used. First FIFO is 32 messages
 * long and is used for reception. Second is used for transmission and is 1
 * message long. Format of message in fifo is described by <CO_CANrxMsg_t> for
 * both: receiving and transmitting messages. However transmitting messages does
 * not use all structure members.
 *
 * \param[in] CANmodule      - pointer to active CANmodule object
 * \param[in] CANbaseAddress - CAN module base address, usually provided by RTOS HAL
 * \param[in] rxArray        - RX buffer array
 * \param[in] rxSize         - size of RX buffer array
 * \param[in] txArray        - TX bffer array
 * \param[in] txSize         - size of TX buffer array
 * \param[in] CANbitRate     - desired CAN bit rate, see link for more details:
 *                             http://www.mkt-sys.de/MKT-CD/upt/help/btr_calculation.htm
 *
 * \retval - CO_ERROR_NO                - operation completed successfully
 * \retval - CO_ERROR_ILLEGAL_ARGUMENT  - error in function arguments
 * \retval - CO_ERROR_OUT_OF_MEMORY     - not enough masks for configuration
 */
CO_ReturnError_t CO_CANmodule_init(
    CO_CANmodule_t         *CANmodule,
    CAN_TypeDef            *CANbaseAddress,
    CO_CANrx_t              rxArray[],
    uint16_t                rxSize,
    CO_CANtx_t              txArray[],
    uint16_t                txSize,
    uint16_t                CANbitRate);


/**
 * Switch off CANmodule.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 */
void CO_CANmodule_disable(CO_CANmodule_t *CANmodule);

/**
 * \brief reads CAN identifier from received message
 *
 * \param[in] rxMsg - pointer to received message
 *
 * \return 11-bit CAN standard identifier
 */
uint16_t CO_CANrxMsg_readIdent(CO_CANrxMsg_t *rxMsg);

/**
 * Configure CAN message receive buffer.
 *
 * Function configures specific CAN receive buffer. It sets CAN identifier
 * and connects buffer with specific object. For more information see
 * topic <Reception of CAN messages>.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 * \param[in] index     - index of CAN device receive buffer
 * \param[in] ident     - 11-bit standard CAN Identifier
 * \param[in] mask      - 11-bit mask for identifier, usually set to 0x7FF.
 *                        Received message (rcvMsg) will be accepted if the following
 *                        condition is true: (((rcvMsgId ^ ident) & mask) == 0).
 * \param[in] rtr       - if true, 'Remote Transmit Request' messages will be accepted
 * \param[in] object    - CANopen object, to which buffer is connected. It will be used as
 *                        an argument to pFunct. Its type is (void), pFunct will change its
 *                        type back to the correct object type
 * \param[in] pFunct    - Pointer to function, which will be called, if received CAN
 *                        message matches the identifier. It must be fast function.
 *                        See <Receive CAN message function>
 *
 * \retval - CO_ERROR_NO                - operation completed successfully
 * \retval - CO_ERROR_ILLEGAL_ARGUMENT  - error in function arguments
 * \retval - CO_ERROR_OUT_OF_MEMORY     - not enough masks for configuration
 */
CO_ReturnError_t CO_CANrxBufferInit(
    CO_CANmodule_t         *CANmodule,
    uint16_t                index,
    uint16_t                ident,
    uint16_t                mask,
    int8_t                  rtr,
    void                   *object,
    void                  (*pFunct)(void *object, const CO_CANrxMsg_t *message));


/**
 * \brief Configure CAN message transmit buffer.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 * \param[in] index     - index of CAN device transmit buffer to configure
 * \param[in] ident     - 11-bit standard CAN Identifier
 * \param[in] rtr       - if true, 'Remote Transmit Request' messages will be transmitted
 * \param[in] noOfBytes - length of CAN message in bytes (0 to 8 bytes)
 * \param[in] syncFlag  - This flag bit is used for synchronous TPDO messages. If set,
 *                        message will not be sent if curent time is outside synchronous window
 *
 * \return Pointer to CAN transmit message buffer <CO_CANtx_t>.
 *         8 bytes data array inside buffer should be written, before
 *         <CO_CANsend> function is called.
 *         Zero is returned in case of wrong arguments.
 */
CO_CANtx_t *CO_CANtxBufferInit(
    CO_CANmodule_t  *CANmodule,
    uint16_t         index,
    uint16_t         ident,
    uint8_t          rtr,
    uint8_t          noOfBytes,
    uint8_t          syncFlag);

/**
 * Send CAN message.
 *
 * For more information see topic <Transmission of CAN messages>. Before this
 * function <CO_CANtxBufferInit> must be caled, which configures TX message
 * buffer. Also 8 bytes of CAN data should be populated.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 * \param[in] buffer    - pointer to transmit buffer, returned by <CO_CANtxBufferInit>
 *
 * \retval CO_ERROR_NO            - Operation completed successfully.
 * \retval CO_ERROR_TX_OVERFLOW   - Previous message is still waiting, buffer full.
 * \retval CO_ERROR_TX_PDO_WINDOW - Synchronous TPDO is outside window.
 */
CO_ReturnError_t CO_CANsend(
    CO_CANmodule_t  *CANmodule,
    CO_CANtx_t      *buffer);


/**
 * Clear all synchronous TPDOs from CAN module transmit buffers.
 *
 * CANopen allows synchronous PDO communication only inside time between SYNC
 * message and SYNC Window. If time is outside this window, new synch. PDOs
 * must not be sent and all pending sync TPDOs, which may be on CAN TX buffers,
 * must be cleared.
 *
 * This function checks (and aborts transmission if necessary) CAN TX buffers
 * when it is called. Function should be called by application in the moment,
 * when SYNC time was just passed out of synchronous window.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 */
void CO_CANclearPendingSyncPDOs(CO_CANmodule_t *CANmodule);


/**
 * Verify all errors of CAN module.
 * Function is called directly from <CO_emergency_process> function.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 */
void CO_CANverifyErrors(CO_CANmodule_t *CANmodule);


/**
 * CAN interrupt to receive CAN messages.
 *
 * Function must be called directly from _C1Interrupt or _C2Interrupt with
 * high priority. For more information see topics: <Reception of CAN messages>
 * and <Transmission of CAN messages>.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 *
 * \return TRUE if function completed succesfully
 */
bool_t CO_CANinterrupt_Rx(CO_CANmodule_t *CANmodule);

/**
 * CAN interrupt to transmit CAN messages.
 *
 * Function must be called directly from _C1Interrupt or _C2Interrupt with
 * high priority. For more information see topics: <Reception of CAN messages>
 * and <Transmission of CAN messages>.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 */
void CO_CANinterrupt_Tx(CO_CANmodule_t *CANmodule);

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif /* _CO_DRIVER_H */
