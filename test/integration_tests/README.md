# CANopen Driver Integration Test
> UBC Sailbot

## Introduction
The fact that drivers depend on the hardware interface makes it difficult to make unit tests for them. We've instead made integration tests that integrate the HAL and the CANopen driver in order to test basic functionality.

The underlying CAN network is in loopback (debug) mode, allowing use to use only one STM32F4 board to test the driver.

There are multiple tests that test for transmitting and receiving functionality, and we try to verify that what is sent is the same as what is received. Using a serial monitor, the contents of the transmit and receive buffer are inspected to make sure that they have the correct bytes.

Alternatively, you can set breakpoints in the code to closely inspect register values and such.

## What you need
1. STM32F4 Discovery board
2. Mini-B USB cable (to power up the board and connect to its on-chip debugger)
3. Micro-USB cable (to connect to the board's serial port for debug logging)

## Setup, build and load
First, make sure that `DRIVER_TEST` is defined (uncomment it if it is commented out) in `CO_driver.h` before running this test. This makes sure that the CAN peripherals on the board communicate in loopback mode, meaning that you don't need two boards to run tests.

1. Run `make` to compile
2. Run `openocd -f stm32f4.cfg` in one terminal window to connect to the board's on-chip debugger
3. Run `arm-none-eabi-gdb build/ada2.elf` in another terminal window. A GDB prompt will show up, so type the following:
    * `tar ext :3333`
    * `mon reset halt`
    * `load`
    * `run` to run the executable

## Set up serial monitor
We first want to know what serial monitor to interface with. To do this, run `ls -ctlr /dev/tty*` in another terminal window and take note of the most recently modified file. This is the serial interface we'll connect to.

Assuming we get `/dev/ttyACM1` as the most recently modified, user your preferred terminal emulator (like CoolTerm for Mac, or PuTTy for Windows) to interface with it. For example, if you use the command line application `screen`, run the command:
    `screen /dev/ttyACM1`
or use your own terminal emulator application to connect to this serial port.

This will bring you to a terminal-like screen where the STM32F4 can print out logging info, and where you can input some commands.

## Run tests
To run the tests, you must be in the serial monitor. Simply run `start` to run the tests. After each test, the contents of the transmit and receive buffer will be printed on screen, as well as an indicator of whether they are equal or not.
