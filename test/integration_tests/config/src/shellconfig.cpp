#include "hal.h"
#include "chprintf.h"

#include "shellconfig.hpp"
#include "usbconfig.hpp"
#include "thread_classes.hpp"

void start(BaseSequentialStream *chp, int argc, char *argv[])
{
  (void)argc;
  (void)argv;
  chprintf(chp, "Starting tests..\r\n\r\n");

  /* Test 1 */
  chprintf(chp, "===============================================\r\n");
  chprintf(chp, "Test 1: Basic CAN functionality\r\n");
  basic_fxn_transmit_thread.start(NORMALPRIO + 10);
  basic_fxn_receive_thread.start(NORMALPRIO + 10);
  basic_fxn_transmit_thread.wait();
  basic_fxn_receive_thread.wait();
  chprintf(chp, "Test 1 END\r\n\r\n");
  chprintf(chp, "===============================================\r\n");

  chThdSleepMilliseconds(1000);

  /* Test 2 */
  chprintf(chp, "===============================================\r\n");
  chprintf(chp, "Test 2: CANtx-related functions\r\n");
  tx_thd.start(NORMALPRIO + 10);
  rcv_after_tx_interrupt.start(NORMALPRIO + 10);
  tx_thd.wait();
  rcv_after_tx_interrupt.wait();
  chprintf(chp, "Test 2 END\r\n\r\n");
  chprintf(chp, "===============================================\r\n");

  chThdSleepMilliseconds(1000);

  /* Test 3 */
  chprintf(chp, "===============================================\r\n");  
  chprintf(chp, "Test 3: CO_CANsend() test\r\n");
  send_thd.start(NORMALPRIO + 10);
  rcv_after_send_thd.start(NORMALPRIO + 10);
  send_thd.wait();
  rcv_after_send_thd.wait();
  chprintf(chp, "Test 3 END\r\n\r\n");
  chprintf(chp, "===============================================\r\n");

  chThdSleepMilliseconds(1000);

  /* Test 4 */
  chprintf(chp, "===============================================\r\n");
  chprintf(chp, "Test 4: CANrx-related functions\r\n");
  transmit_thd.start(NORMALPRIO + 10);
  rx_thd.start(NORMALPRIO + 10);
  transmit_thd.wait();
  rx_thd.wait();
  chprintf(chp, "Test 4 END\r\n\r\n");
  chprintf(chp, "===============================================\r\n");

  chThdSleepMilliseconds(1000);

  /* Test 5 */
  chprintf(chp, "===============================================\r\n");
  chprintf(chp, "Test 5: CO_CANsend and CANrx integration\r\n");
  transmit_to_rx.start(NORMALPRIO + 10);
  rx_after_send.start(NORMALPRIO + 10);
  transmit_to_rx.wait();
  rx_after_send.wait();
  chprintf(chp, "Test 5 END\r\n\r\n");
  chprintf(chp, "===============================================\r\n");

  chThdSleepMilliseconds(1000);

  /* Test 6 */
  chprintf(chp, "===============================================\r\n");
  chprintf(chp, "Test 6: Full integration\r\n");
  tx_integration.start(NORMALPRIO + 10);
  rx_integration.start(NORMALPRIO + 10);
  tx_integration.wait();
  rx_integration.wait();
  chprintf(chp, "Test 6 END\r\n\r\n");
  chprintf(chp, "===============================================\r\n");
}

const ShellCommand shellcmds[] =
{
  {"start", start},
  {NULL, NULL}
};

const ShellConfig shellcfg =
{
  (BaseSequentialStream*)&SDU1,
  shellcmds
};