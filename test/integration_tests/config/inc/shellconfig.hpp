#ifndef SHELL_CONF
#define SHELL_CONF

#include "hal.h"
#include "shell.h"

// #include "thread_classes.hpp"

#define SHELL_WA_SIZE THD_WORKING_AREA_SIZE(2048)

/**
 * \brief Function that gets called after "start" command in serial monitor,
 *        this starts the test sequence.
 *
 * \param[in] chp pointer to output stream
 */
void start(BaseSequentialStream *chp, int argc, char *argv[]);

extern const ShellCommand shellcmds[];

extern const ShellConfig shellcfg;

#endif /* SHELL_CONF */
