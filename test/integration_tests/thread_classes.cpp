#include "mock_functions.h"
#include "chprintf.h"

#include "usbconfig.hpp"
#include "thread_classes.hpp"

#define RCV_EVT_MASK 0
#define RX_BUF_SIZE 5U
#define TX_BUF_SIZE 5U
#define CAN_BIT_RATE 1000U

using namespace chibios_rt;

static void
PrintArrayByteContents(uint8_t array[], size_t size)
{
  for(size_t i = 0 ; i < size ; i++) {
    chprintf((BaseSequentialStream*)&SDU1, "0x%02x ", array[i]);
  }
  chprintf((BaseSequentialStream*)&SDU1, "\r\n\r\n");
}

void CanBasicTransmitThd::main(void)
{
  CANTxFrame txmsg;

  setName("transmitter");
  txmsg.IDE = CAN_IDE_STD;
  txmsg.SID = 0x000001AA;
  txmsg.RTR = rtr;
  txmsg.DLC = 8;
  txmsg.data8[0] = 0xDE;
  txmsg.data8[1] = 0xAD;
  txmsg.data8[2] = 0xBE;
  txmsg.data8[3] = 0xEF;
  txmsg.data8[4] = 0xDE;
  txmsg.data8[5] = 0xAD;
  txmsg.data8[6] = 0xBE;
  txmsg.data8[7] = 0xEF;

  /* print out frame info into serial monitor */
  chprintf((BaseSequentialStream*)&SDU1, "Transmitted SID : 0x%08x\r\n", txmsg.SID);
  chprintf((BaseSequentialStream*)&SDU1, "Transmitted RTR : %s\r\n",
      txmsg.RTR == CAN_RTR_REMOTE ? "REMOTE" : "DATA");
  chprintf((BaseSequentialStream*)&SDU1, "Transmitted contents : ");
  PrintArrayByteContents(txmsg.data8, sizeof(txmsg.data8));

  while (!shouldTerminate()) {
    canTransmit(_co_module->CANdrv, 0, &txmsg, MS2ST(100));
    sleep(MS2ST(500));
    /* only transmit once, then exit */
    exit((msg_t)0);
  }
}

CanBasicTransmitThd::CanBasicTransmitThd(CO_CANmodule_t *co_module_in, uint8_t rtr_in)
    : BaseStaticThread<256>() 
{
  _co_module = co_module_in;
  rtr = rtr_in;
}



void CanBasicReceiveThd::main(void)
{
  event_listener_t el;
  CANRxFrame rxmsg;

  setName("receiver");
  chEvtRegister(&_co_module->CANdrv->rxfull_event, &el, 0);
  while (!shouldTerminate()) {
    if (waitAnyEventTimeout(ALL_EVENTS, MS2ST(100)) == 0) {
      continue;
    }

    while (canReceive(_co_module->CANdrv, 0,
                      &rxmsg, TIME_IMMEDIATE) == MSG_OK) {
      /* print out RX frame contents */
      chprintf((BaseSequentialStream*)&SDU1, "Received SID : 0x%08x\r\n", rxmsg.SID);
      chprintf((BaseSequentialStream*)&SDU1, "Received RTR : %s\r\n",
          rxmsg.RTR == CAN_RTR_REMOTE ? "REMOTE" : "DATA");
      chprintf((BaseSequentialStream*)&SDU1, "Received contents : ");
      PrintArrayByteContents(rxmsg.data8, sizeof(rxmsg.data8));

      /* only receive once, then exit */
      chEvtUnregister(&_co_module->CANdrv->rxfull_event, &el);
      exit((msg_t)0);
    }
  }
}

CanBasicReceiveThd::CanBasicReceiveThd(CO_CANmodule_t *co_module_in)
    : BaseStaticThread<256>()
{
  _co_module = co_module_in;
}


void CanOpenTxThd::main(void)
{
  CO_CANtxBufferInit(
      _co_module,      /* CANopen module */
      0U,              /* Tx buffer index */
      0x01AA,          /* CAN SID */
      CAN_RTR_REMOTE,  /* RTR bit */
      8U,              /* number of bytes to send */
      0U);             /* SYNC flag */

  /* initialize Tx buffer data */
  _co_module->txArray[0U].data[0] = 0xDE;
  _co_module->txArray[0U].data[1] = 0xAD;
  _co_module->txArray[0U].data[2] = 0xBE;
  _co_module->txArray[0U].data[3] = 0xEF;
  _co_module->txArray[0U].data[4] = 0xDE;
  _co_module->txArray[0U].data[5] = 0xAD;
  _co_module->txArray[0U].data[6] = 0xBE;
  _co_module->txArray[0U].data[7] = 0xEF;

  /* set bufferFull flag and CANtxCount (number of pending
     transmit messages) so that function actually sends data */
  _co_module->txArray[0U].bufferFull = TRUE;
  _co_module->CANtxCount = 1;

  /* print out frame info into serial monitor */
  chprintf((BaseSequentialStream*)&SDU1, "Transmitted SID : 0x%08x\r\n", _co_module->txArray[0U].ident);
  chprintf((BaseSequentialStream*)&SDU1, "Transmitted RTR : %s\r\n",
      _co_module->txArray[0U].RTR == CAN_RTR_REMOTE ? "REMOTE" : "DATA");
  chprintf((BaseSequentialStream*)&SDU1, "Transmitted contents : ");
  PrintArrayByteContents(_co_module->txArray[0U].data, sizeof(_co_module->txArray[0U].data));

  if(!shouldTerminate()) {
    /* off you go billy */
    CO_CANinterrupt_Tx(_co_module);
    /* only transmit once, and exit */
    exit((msg_t)0);
  }
}

CanOpenTxThd::CanOpenTxThd(CO_CANmodule_t *co_module_in)
    : BaseStaticThread<256>()
{
  _co_module = co_module_in;
}



void CanOpenRxThd::main(void)
{
  event_listener_t el;
  void* obj = (void*)0xDEAD;

  /* initialize RX buffer */
  CO_CANrxBufferInit(
      _co_module,     /* CANopen module */
      0U,             /* RX buffer index */
      0x01AA,         /* SID */
      0x07FF,         /* SID mask */
      1,              /* RTR flag */
      obj,            /* dummy pointer */
      mock_function); /* pointer to mock function */

  setName("receiver");
  chEvtRegister(&_co_module->CANdrv->rxfull_event, &el, 0);
  while (!shouldTerminate()) {
    if (waitAnyEventTimeout(ALL_EVENTS, MS2ST(100)) == 0) {
      continue;
    }
    CO_CANinterrupt_Rx(_co_module);

    /* only receive once, then exit */
    chEvtUnregister(&_co_module->CANdrv->rxfull_event, &el);
    exit((msg_t)0);
  }

  chEvtUnregister(&_co_module->CANdrv->rxfull_event, &el);
}

CanOpenRxThd::CanOpenRxThd(CO_CANmodule_t *co_module_in)
    : BaseStaticThread<256>()
{
  _co_module = co_module_in;
}



void CanOpenSendThd::main(void)
{
  CO_CANtx_t *tx_buff;

  tx_buff = CO_CANtxBufferInit(
      _co_module,      /* CANopen module */
      0U,              /* Tx buffer index */
      0x01AA,          /* CAN SID */
      CAN_RTR_REMOTE,  /* RTR bit */
      8U,              /* number of bytes to send */
      0U);             /* SYNC flag */

  /* initialize Tx buffer data */
  _co_module->txArray[0U].data[0] = 0xDE;
  _co_module->txArray[0U].data[1] = 0xAD;
  _co_module->txArray[0U].data[2] = 0xBE;
  _co_module->txArray[0U].data[3] = 0xEF;
  _co_module->txArray[0U].data[4] = 0xDE;
  _co_module->txArray[0U].data[5] = 0xAD;
  _co_module->txArray[0U].data[6] = 0xBE;
  _co_module->txArray[0U].data[7] = 0xEF;

  chprintf((BaseSequentialStream*)&SDU1, "Transmitted SID : 0x%04x\r\n", _co_module->txArray[0U].ident);
  chprintf((BaseSequentialStream*)&SDU1, "Transmitted RTR : %s\r\n",
      _co_module->txArray[0U].RTR == CAN_RTR_REMOTE ? "REMOTE" : "DATA");
  chprintf((BaseSequentialStream*)&SDU1, "Transmitted contents : ");
  PrintArrayByteContents(_co_module->txArray[0U].data, sizeof(_co_module->txArray[0U].data));

  if(!shouldTerminate()) {
    CO_CANsend(_co_module, tx_buff);
    sleep(MS2ST(500));
    exit((msg_t)0);
  }
}

CanOpenSendThd::CanOpenSendThd(CO_CANmodule_t *co_module_in) : BaseStaticThread<256>()
{
  _co_module = co_module_in;
}

/* CANopen objects */
CO_CANmodule_t can_1;
CO_CANrx_t rx_array_1[RX_BUF_SIZE];
CO_CANtx_t tx_array_1[TX_BUF_SIZE];

/**
 * Test 1 threads: Basic CAN functionality
 */
CanBasicTransmitThd basic_fxn_transmit_thread(&can_1, CAN_RTR_DATA);
CanBasicReceiveThd basic_fxn_receive_thread(&can_1);

/**
 * Test 2 threads: CANopen Tx-related functions
 *                 ( CO_CANtxBufferInit and CO_CANinterrupt_Tx )
 */
CanOpenTxThd tx_thd(&can_1);
CanBasicReceiveThd rcv_after_tx_interrupt(&can_1);

/**
 * Test 3 threads: CANopen CO_CANsend() test
 */
CanOpenSendThd send_thd(&can_1);
CanBasicReceiveThd rcv_after_send_thd(&can_1);

/**
 * Test 4 threads: CANopen Rx-related functions
 *                 ( CO_CANrxBufferInit and CO_CANinterrupt_Rx )
 */
CanBasicTransmitThd transmit_thd(&can_1, CAN_RTR_REMOTE);
CanOpenRxThd rx_thd(&can_1);

/**
 * Test 5 threads: CANopen CO_CANsend() and Rx-related functions test
 */
CanBasicTransmitThd transmit_to_rx(&can_1, CAN_RTR_REMOTE);
CanOpenRxThd rx_after_send(&can_1);

/**
 * Test 6 threads: Full integration
 */
CanOpenTxThd tx_integration(&can_1);
CanOpenRxThd rx_integration(&can_1);