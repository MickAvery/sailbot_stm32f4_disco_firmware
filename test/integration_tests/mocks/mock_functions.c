#include "hal.h"
#include "chprintf.h"
#include "mock_functions.h"

#include "usbconfig.hpp"

void mock_function(void* object, const CO_CANrxMsg_t* message)
{
  size_t i;
  (void)object;
  (void)message;
  chprintf((BaseSequentialStream*)&SDU1, "RX interrupt handler invoked, receiving successfull\r\n");
  chprintf((BaseSequentialStream*)&SDU1, "Rx frame SID : 0x%08x\r\n", message->ident);
  chprintf((BaseSequentialStream*)&SDU1, "Rx frame RTR : %s\r\n", message->RTR == 1 ? "REMOTE" : "DATA");
  chprintf((BaseSequentialStream*)&SDU1, "Rx frame contents : ");

  for(i = 0 ; i < ( sizeof(message->data) / sizeof(message->data[0]) ) ; i++) {
    chprintf((BaseSequentialStream*)&SDU1, "0x%02x ", message->data[i]);
  }
  chprintf((BaseSequentialStream*)&SDU1, "\r\n");
}