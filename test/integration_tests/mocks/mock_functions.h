/**
 *
 **/

#include "CO_driver.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief Test function to pass to CANopenNode functions
 *
 * \param[in] object  - pointer to object
 * \param[in] message - received message to process
 **/
void mock_function(void* object, const CO_CANrxMsg_t* message);

#ifdef __cplusplus
} /* extern "C" */
#endif
