#include "CO_driver.h"
#include "CO_Emergency.h"

/**
 * \brief mock implementation of CO_errorReport()
 **/
void CO_errorReport(CO_EM_t *em, const uint8_t errorBit, const uint16_t errorCode, const uint32_t infoCode)
{
  (void)em;
  (void)errorBit;
  (void)errorCode;
  (void)infoCode;
}
