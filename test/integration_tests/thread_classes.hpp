#include "hal.h"
#include "CO_driver.h"

#include "ch.hpp"

#define RCV_EVT_MASK 0
#define RX_BUF_SIZE 5U
#define TX_BUF_SIZE 5U
#define CAN_BIT_RATE 1000U

using namespace chibios_rt;

/**
 * Thread for CAN transmitters
 */
class CanBasicTransmitThd : public BaseStaticThread<256>
{
 private:
  CO_CANmodule_t *_co_module;
  uint8_t rtr;

 protected:
  virtual void main(void);

 public:
  CanBasicTransmitThd(CO_CANmodule_t *co_module_in, uint8_t rtr_in);
};

/**
 * Thread for CAN receivers
 */
class CanBasicReceiveThd : public BaseStaticThread<256>
{
 private:
  CO_CANmodule_t *_co_module;

 protected:
  virtual void main(void);

 public:
  CanBasicReceiveThd(CO_CANmodule_t *co_module_in);
};

/**
 * Thread for Tx-related functions
 */
class CanOpenTxThd : public BaseStaticThread<256>
{
 private:
  CO_CANmodule_t *_co_module;

 protected:
  virtual void main(void);

 public:
  CanOpenTxThd(CO_CANmodule_t *co_module_in);
};

/**
 * Thread for Rx-related functions
 */
class CanOpenRxThd : public BaseStaticThread<256>
{
 private:
  CO_CANmodule_t *_co_module;

 protected:
  virtual void main(void);

 public:
  CanOpenRxThd(CO_CANmodule_t *co_module_in);
};

class CanOpenSendThd : public BaseStaticThread<256>
{
 private:
  CO_CANmodule_t *_co_module;

 protected:
  virtual void main(void);

 public:
  CanOpenSendThd(CO_CANmodule_t *co_module_in);
};

/* CANopen objects */
extern CO_CANmodule_t can_1;
extern CO_CANrx_t rx_array_1[RX_BUF_SIZE];
extern CO_CANtx_t tx_array_1[TX_BUF_SIZE];

/**
 * Test 1 threads: Basic CAN functionality
 */
extern CanBasicTransmitThd basic_fxn_transmit_thread;
extern CanBasicReceiveThd basic_fxn_receive_thread;

/**
 * Test 2 threads: CANopen Tx-related functions
 *                 ( CO_CANtxBufferInit and CO_CANinterrupt_Tx )
 */
extern CanOpenTxThd tx_thd;
extern CanBasicReceiveThd rcv_after_tx_interrupt;

/**
 * Test 3 threads: CANopen CO_CANsend() test
 */
extern CanOpenSendThd send_thd;
extern CanBasicReceiveThd rcv_after_send_thd;

/**
 * Test 4 threads: CANopen Rx-related functions
 *                 ( CO_CANrxBufferInit and CO_CANinterrupt_Rx )
 */
extern CanBasicTransmitThd transmit_thd;
extern CanOpenRxThd rx_thd;

/**
 * Test 5 threads: CANopen CO_CANsend() and Rx-related functions test
 */
extern CanBasicTransmitThd transmit_to_rx;
extern CanOpenRxThd rx_after_send;

/**
 * Test 6 threads: Full integration
 */
extern CanOpenTxThd tx_integration;
extern CanOpenRxThd rx_integration;
