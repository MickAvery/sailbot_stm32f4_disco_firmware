#include "ch.hpp"
#include "hal.h"
#include "ch_test.h"
#include "shell.h"
#include "CO_driver.h"
#include "CO_OD.h"

#include "usbconfig.hpp"
#include "shellconfig.hpp"
#include "thread_classes.hpp"


#ifndef DRIVER_TEST
  #error "set the DRIVER_TEST macro to true to compile test"
#endif

using namespace chibios_rt;

/*
 * Application entry point.
 */
int main(void)
{
  CO_ReturnError_t can_err_1 = CO_ERROR_NO;

  /* System initializations */
  halInit();
  System::init();

  /* initialize serial-to-usb driver */
  sduObjectInit(&SDU1);
  sduStart(&SDU1, &serusbcfg);

  /* initialize usb driver */
  usbDisconnectBus(serusbcfg.usbp);
  chThdSleepMilliseconds(1500);
  usbStart(serusbcfg.usbp, &usbcfg);
  usbConnectBus(serusbcfg.usbp);

  /* initialize shell and dynamic thread */
  shellInit();

  /* Initialize CAN module */
  can_err_1 = CO_CANmodule_init(&can_1,
      CAND1.can,
      rx_array_1, RX_BUF_SIZE,
      tx_array_1, TX_BUF_SIZE,
      CAN_BIT_RATE);

  /* error checking */
  if(can_err_1 == CO_ERROR_ILLEGAL_ARGUMENT)
  {
    return 0;
  }

  while(1){
    /* just loop, doing nothing */
    // BaseThread::sleep(MS2ST(500));
    if (SDU1.config->usbp->state == USB_ACTIVE) {
      thread_t *shelltp = chThdCreateFromHeap(NULL, SHELL_WA_SIZE,
                                              "shell", NORMALPRIO + 1,
                                              shellThread, (void *)&shellcfg);
      chThdWait(shelltp); /* Waiting termination. */
    }
    chThdSleepMilliseconds(1000);
  }

  /* disable CANopen modules */
  CO_CANmodule_disable(&can_1);

  return 0;
}
