# ChibiOS and CANopenNode
> Sailbot Control Software Team

### Folder Structure
    .
    ├── **inc**                             # Directory for all header files
    │   └── **co**                          # CANopen header files
    │       ├── CO_OD.h                     # CANopen object dictionary header file
    │       └── CO_driver.h                 # CANopen driver header file for STM32F4-Disco
    │
    ├── **src**                             # Directory for all source files
    │   ├── **chibios**                     # User-made ChibiOS source files
    │   │   └── main.cpp                    # Main ChibiOS executable
    │   └── **co**                          # CANopen drivers and Object Dictionary source files
    │       ├── CO_OD.c                     # Object Dictionary source
    │       └── CO_driver.c                 # CANopen driver source for STM32F4-Disco
    │
    ├── **test**                            # Tests for CANopen
    │   ├── **integration_test**            # Integration testing for CANopen driver
    │   └── **unit_test**                   # Unit tests for project
    │
    ├── third_party                         # All third-party libraries
    │   ├── CANopenNode                     # CANopenNode submodule
    │   ├── ChibiOS                         # ChibiOS submodule
    │   └── Object_Dictionary_Editor        # Object Dictionary Editory to generate OD.[c/h] (object dictionaries used by CANopen)
    │
    ├── chconf.h                            # ChibiOS kernel config file
    ├── halconf.h                           # Hardware abstraction layer config file (activate/deactivate hardware peripherals)
    ├── mcuconf.h                           # Hardware-specific config file (in our case, STM32F407)
    ├── Makefile
    └── README.md                           # What you're looking at right now

### Initialization
1. Run `git submodule update --init` to pull all submodules
2. Run `make` to compile

### Testing
Head on over to `test/integration_test` to run the integration tests for the CANopen driver. More details will be found in the `README` in that directory.
