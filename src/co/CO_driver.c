/* Copyright 2017 UBC Sailbot */

#include <string.h>
#include <stdlib.h>

#include "CO_driver.h"
#include "CO_Emergency.h"

/**
 * \brief Helper function to copy data from CANRxFrame struct provided by ChibiOS API
 *        into a CO_CANrxMsg_t struct that can be passed to CANopenNode API
 * \param[out] co_rx_msg CANopenNode RX message pointer to copy data to
 * \param[in] co_rx_frame ChibiOS RX frame pointer to copy dtaa from
 * \return TRUE if copy is successful
 *
 * \notapi
 */
static bool_t
_co_rx_msg_convert(CO_CANrxMsg_t *co_rx_msg, CANRxFrame *co_rx_frame)
{
  if(co_rx_msg == NULL || co_rx_frame == NULL) {
    return FALSE;
  }

  co_rx_msg->RTR = co_rx_frame->RTR;
  co_rx_msg->FMI = co_rx_frame->FMI;
  co_rx_msg->TIME = co_rx_frame->TIME;
  co_rx_msg->DLC = co_rx_frame->DLC;
  if( (co_rx_msg->IDE = co_rx_frame->IDE) == 1U ) {
    co_rx_msg->ExtId = co_rx_frame->EID;
  } else {
    co_rx_msg->ident = co_rx_frame->SID;
  }

  (void)memcpy(co_rx_msg->data, co_rx_frame->data8, 8U);

  return TRUE;
}

/**
 * \brief Helper function to copy data from CO_CANtx_t struct provided by CANopenNode API
 *        into a CANTxFrame struct that can be passed to ChibiOS CAN API calls
 * \param[out] can_tx_frame object to copy data to
 * \param[in] co_tx_frame object to copy data from
 * \return TRUE if copy is successful
 *
 * \notapi
 */
static bool_t
_co_tx_msg_convert(CANTxFrame *can_tx_frame, CO_CANtx_t *co_tx_frame)
{
  if( (can_tx_frame == NULL) || (co_tx_frame == NULL) ) {
    return FALSE;
  }

  can_tx_frame->DLC = co_tx_frame->DLC;
  can_tx_frame->RTR = co_tx_frame->RTR;
  can_tx_frame->SID = co_tx_frame->ident;
  can_tx_frame->IDE = 0U;
  (void)memcpy(can_tx_frame->data8, co_tx_frame->data, 8U);

  return TRUE;
}

/**
 * \brief send a CAN TX frame out to the send queue
 * \param CANmodule - pointer to active CANopen module object
 * \param buffer - pointer to TX buffer to send to queue
 * \param transmit_mailbox - mailbox in which to send frame to
 * \return TRUE if operation successful
 *
 * \notapi
 */
static bool_t
_can_send_to_module(CO_CANmodule_t *CANmodule, CO_CANtx_t *buffer, uint8_t transmit_mailbox)
{
  /* TODO: maybe this should return bool to specify if sent successfully? */
  CANTxFrame can_tx_frame;

  if(transmit_mailbox <= 3U) {
    /* TODO: error checking! */
    (void)_co_tx_msg_convert(&can_tx_frame, buffer);
    /* TODO: change timeout value */
    canTransmit(CANmodule->CANdrv,
        (canmbx_t) transmit_mailbox,
        &can_tx_frame,
        MS2ST(100));
  }

  return TRUE;
}

/**
 * \brief Get free Tx mailbox
 * \param[in] CANmodule - pointer to CANopen module
 * \return mailbox (value of 0-3) if there is a free one,
 *         -1 if no mailbox found
 *
 * \notapi
 */
static int8_t
_get_free_tx_buff(CO_CANmodule_t *CANmodule)
{
  canmbx_t tx_mbx;

  for (tx_mbx = 0U; tx_mbx <= CAN_TX_MAILBOXES ; tx_mbx++) {
    if( can_lld_is_tx_empty(CANmodule->CANdrv, tx_mbx) ) {
      return (int8_t) tx_mbx;
    };
  }

  return -1;
}

/**
 * \brief determine CAN BTR prescaler value based on desired baud rate
 * \note See section 32.7.7 of STM32F4 reference manual for more bit timing details 
 * \param[in]  baudrate  - desired CANopen baudrate
 * \param[out] prescaler - pointer to baud rate prescaler
 * \return TRUE if the desired baud rate is valid and operation completed successfully,
 *         FALSE otherwise
 *
 * \notapi
 */
static bool_t
_get_btr_prescaler(uint16_t baudrate, uint16_t* prescaler)
{
  if( prescaler == NULL ) {
    return FALSE;
  } else if( baudrate == 1000U ) {
    *prescaler = 3;
    return TRUE;
  } else if( baudrate == 800U ) {
    *prescaler = 4;
    return TRUE;
  } else if( baudrate == 500U ) {
    *prescaler = 7;
    return TRUE;
  } else if( baudrate == 250U ) {
    *prescaler = 15;
    return TRUE;
  } else if( baudrate == 125U ) {
    *prescaler = 31;
    return TRUE;
  } else if( baudrate == 50U ) {
    *prescaler = 79;
    return TRUE;
  } else if( baudrate == 20U ) {
    *prescaler = 199;
    return TRUE;
  } else if( baudrate == 10U ) {
    *prescaler = 399;
    return TRUE;
  } else {
    return FALSE;
  }
}

/********************************************************
 *************** Functions open to API ******************
 ********************************************************/

/**
 * \brief requests CAN configuration (stopped) mode and *wait* untill it is set
 *
 * \param[in] CANbaseAddress
 */
void CO_CANsetConfigurationMode(int32_t CANbaseAddress)
{
  /**
   * Configuration is done when CO_CANmodule_init()
   * calls canStart();
   */
  (void)CANbaseAddress;
}

/**
 * \brief requests CAN normal (opearational) mode and *wait* untill it is set
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 */
void CO_CANsetNormalMode(CO_CANmodule_t *CANmodule)
{
  CANmodule->CANnormal = true;
}

/**
 * Initialize CAN module object.

 * Function must be called in the communication reset section. CAN module must
 * be in Configuration Mode before.
 * PIC32MX CAN FIFO configuration: Two FIFOs are used. First FIFO is 32 messages
 * long and is used for reception. Second is used for transmission and is 1
 * message long. Format of message in fifo is described by <CO_CANrxMsg_t> for
 * both: receiving and transmitting messages. However transmitting messages does
 * not use all structure members.
 *
 * \param[out] CANmodule      - pointer to active CANmodule object
 * \param[in] CANbaseAddress  - CAN module base address, usually provided by RTOS HAL
 * \param[in] rxArray         - RX buffer array
 * \param[in] rxSize          - size of RX buffer array
 * \param[in] txArray         - TX bffer array
 * \param[in] txSize          - size of TX buffer array
 * \param[in] CANbitRate      - desired CAN bit rate, see link for more details:
 *                              http://www.chibios.com/forum/viewtopic.php?f=35&t=3224
 *
 * \retval - CO_ERROR_NO                - operation completed successfully
 * \retval - CO_ERROR_ILLEGAL_ARGUMENT  - error in function arguments
 * \retval - CO_ERROR_OUT_OF_MEMORY     - not enough masks for configuration
 */
CO_ReturnError_t CO_CANmodule_init(
    CO_CANmodule_t         *CANmodule,
    CAN_TypeDef            *CANbaseAddress,
    CO_CANrx_t              rxArray[],
    uint16_t                rxSize,
    CO_CANtx_t              txArray[],
    uint16_t                txSize,
    uint16_t                CANbitRate)
{
  uint16_t prescaler = 3U; /* prescaler defaults to 3, resulting in baud rate of 1000kbps */

  if( (CANmodule == NULL) || 
      (rxArray == NULL)   || 
      (txArray == NULL) ) {
    return CO_ERROR_ILLEGAL_ARGUMENT;
  } else if( _get_btr_prescaler(CANbitRate, &prescaler) == FALSE ) {
    return CO_ERROR_ILLEGAL_BAUDRATE;;
  }

  /**
   * NOTE: read for more info:
   *     http://www.chibios.com/forum/viewtopic.php?f=35&t=3224
   */
  CANConfig cancfg = 
  {
    CAN_MCR_ABOM | CAN_MCR_AWUM | CAN_MCR_TXFP,      /* MCR config */
#ifdef DRIVER_TEST
    CAN_BTR_LBKM |
#endif
    CAN_BTR_SJW(0) | CAN_BTR_TS2(3) | /* BTR (bit timing) config */
    CAN_BTR_TS1(3) | CAN_BTR_BRP(prescaler)
  };

  CANmodule->CANbaseAddress     = CANbaseAddress;
  CANmodule->CANdrv             = &CAND1;
  CANmodule->rxArray            = rxArray;
  CANmodule->rxSize             = rxSize;
  CANmodule->txArray            = txArray;
  CANmodule->txSize             = txSize;
  CANmodule->CANnormal          = false;
  CANmodule->useCANrxFilters    = false;
  CANmodule->bufferInhibitFlag  = 0;
  CANmodule->firstCANtxMessage  = 1;
  CANmodule->CANtxCount         = 0;
  CANmodule->errOld             = 0;
  CANmodule->em                 = 0;

  size_t i;
  for (i = 0; i < rxSize; i++) {
    CANmodule->rxArray[i].ident = 0;
    CANmodule->rxArray[i].pFunct = 0;
  }

  for (i = 0; i < txSize; i++) {
    CANmodule->txArray[i].bufferFull = 0;
  }

  /* TODO: what are some things we can do to check for errors in canStart? */
  canStart(CANmodule->CANdrv, &cancfg);

  return CO_ERROR_NO;
}

/**
 * Switch off CANmodule.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 */
void CO_CANmodule_disable(CO_CANmodule_t *CANmodule)
{
  /* stop and uninitialize CAN driver */
  canStop(CANmodule->CANdrv);
}

/**
 * \brief reads CAN identifier from received message 
 *
 * \param[in] rxMsg - pointer to received message
 *
 * \return 11-bit CAN standard identifier 
 */
uint16_t CO_CANrxMsg_readIdent(CO_CANrxMsg_t *rxMsg)
{
  return (uint16_t) rxMsg->ident;
}

/**
 * Configure CAN message receive buffer.
 *
 * Function configures specific CAN receive buffer. It sets CAN identifier
 * and connects buffer with specific object. For more information see
 * topic <Reception of CAN messages>.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 * \param[in] index     - index of CAN device receive buffer
 * \param[in] ident     - 11-bit standard CAN Identifier
 * \param[in] mask      - 11-bit mask for identifier, usually set to 0x7FF.
 *                        Received message (rcvMsg) will be accepted if the following
 *                        condition is true: (((rcvMsgId ^ ident) & mask) == 0).
 * \param[in] rtr       - if true, 'Remote Transmit Request' messages will be accepted
 * \param[in] object    - CANopen object, to which buffer is connected. It will be used as
 *                        an argument to pFunct. Its type is (void), pFunct will change its
 *                        type back to the correct object type
 * \param[in] pFunct    - Pointer to function, which will be called, if received CAN
 *                        message matches the identifier. It must be fast function.
 *                        See <Receive CAN message function>
 *
 * \retval - CO_ERROR_NO                - operation completed successfully
 * \retval - CO_ERROR_ILLEGAL_ARGUMENT  - error in function arguments
 * \retval - CO_ERROR_OUT_OF_MEMORY     - not enough masks for configuration
 */
CO_ReturnError_t CO_CANrxBufferInit(
    CO_CANmodule_t         *CANmodule,
    uint16_t                index,
    uint16_t                ident,
    uint16_t                mask,
    int8_t                  rtr,
    void                   *object,
    void                  (*pFunct)(void *object, const CO_CANrxMsg_t *message))
{
  CO_CANrx_t *rx_buffer;

  /* safety */
  if (!CANmodule || !object || !pFunct || index >= CANmodule->rxSize) {
    return CO_ERROR_ILLEGAL_ARGUMENT;
  }

  /* buffer, which will be configured */
  rx_buffer = &CANmodule->rxArray[index];

  /* configure RX buffer */
  rx_buffer->object = object;
  rx_buffer->pFunct = pFunct;
  rx_buffer->ident  = ident;
  rx_buffer->rtr    = rtr;
  rx_buffer->mask   = mask;

  return CO_ERROR_NO;
}

/**
 * \brief Configure CAN message transmit buffer.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 * \param[in] index     - index of CAN device transmit buffer to configure
 * \param[in] ident     - 11-bit standard CAN Identifier
 * \param[in] rtr       - if true, 'Remote Transmit Request' messages will be transmitted
 * \param[in] noOfBytes - length of CAN message in bytes (0 to 8 bytes)
 * \param[in] syncFlag  - This flag bit is used for synchronous TPDO messages. If set,
 *                        message will not be sent if curent time is outside synchronous window
 *
 * \return Pointer to CAN transmit message buffer <CO_CANtx_t>.
 *         8 bytes data array inside buffer should be written, before
 *         <CO_CANsend> function is called.
 *         NULL is returned in case of wrong arguments.
 */
CO_CANtx_t* CO_CANtxBufferInit(
    CO_CANmodule_t *CANmodule,
    uint16_t index,
    uint16_t ident,
    uint8_t rtr,
    uint8_t noOfBytes,
    uint8_t syncFlag)
{
  CO_CANtx_t *tx_buffer;

  /* safety */
  if (!CANmodule || CANmodule->txSize <= index) {
    return NULL;
  }

  /* get and set specific buffer */
  tx_buffer = &CANmodule->txArray[index];
  tx_buffer->ident = ident;
  tx_buffer->RTR = rtr;

  /* write to buffer */
  tx_buffer->DLC = noOfBytes;
  tx_buffer->bufferFull = 0;
  tx_buffer->syncFlag = syncFlag ? 1 : 0;

  return tx_buffer;
}

/**
 * Send CAN message.
 *
 * For more information see topic <Transmission of CAN messages>. Before this
 * function <CO_CANtxBufferInit> must be caled, which configures TX message
 * buffer. Also 8 bytes of CAN data should be populated.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 * \param[in] buffer    - pointer to transmit buffer, returned by <CO_CANtxBufferInit>
 *
 * \retval CO_ERROR_NO            - Operation completed successfully.
 * \retval CO_ERROR_TX_OVERFLOW   - Previous message is still waiting, buffer full.
 * \retval CO_ERROR_TX_PDO_WINDOW - Synchronous TPDO is outside window.
 */
CO_ReturnError_t CO_CANsend(CO_CANmodule_t *CANmodule, CO_CANtx_t *buffer)
{
  CO_ReturnError_t err = CO_ERROR_NO;
  int8_t tx_mbx;

  /* was previous message sent or it is still waiting? */
  if(buffer->bufferFull) {
    if(!CANmodule->firstCANtxMessage) { /* don't set error, if bootup message is still on buffers */
      CO_errorReport((CO_EM_t*)CANmodule->em,
          CO_EM_CAN_TX_OVERFLOW,
          CO_EMC_CAN_OVERRUN,
          0);
    }

    err = CO_ERROR_TX_OVERFLOW;
  }

  /* if CAN TB buffer0 is free, copy message to it */
  tx_mbx = _get_free_tx_buff(CANmodule);
  if( (tx_mbx != -1)  && (CANmodule->CANtxCount == 0) ) {
    CANmodule->bufferInhibitFlag = buffer->syncFlag;
    /* TODO: figure out what to do with TX RTR bit */
    (void)_can_send_to_module(CANmodule, buffer, tx_mbx);
  }
  /* if no buffer is free, message will be sent by interrupt */
  else {
    buffer->bufferFull = TRUE;
    CANmodule->CANtxCount++;
  }

  return err;
}

/**
 * Clear all synchronous TPDOs from CAN module transmit buffers.
 *
 * CANopen allows synchronous PDO communication only inside time between SYNC
 * message and SYNC Window. If time is outside this window, new synch. PDOs
 * must not be sent and all pending sync TPDOs, which may be on CAN TX buffers,
 * must be cleared.
 *
 * This function checks (and aborts transmission if necessary) CAN TX buffers
 * when it is called. Function should be called by application in the moment,
 * when SYNC time was just passed out of synchronous window.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 */
void CO_CANclearPendingSyncPDOs(CO_CANmodule_t *CANmodule)
{
  DISABLE_INTERRUPTS();

  if (CANmodule->bufferInhibitFlag) {
    CANmodule->CANdrv->can->TSR |= (CAN_TSR_ABRQ0 | CAN_TSR_ABRQ1 | CAN_TSR_ABRQ2);
    ENABLE_INTERRUPTS();

    /* TODO: Figure out correct error code */
    CO_errorReport((CO_EM_t*) CANmodule->em, CO_EM_TPDO_OUTSIDE_WINDOW, CO_EMC_GENERIC, 0);
  } else {
    ENABLE_INTERRUPTS();
  }
}

/**
 * Verify all errors of CAN module.
 * Function is called directly from <CO_emergency_process> function.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 */
void CO_CANverifyErrors(CO_CANmodule_t *CANmodule)
{
  uint32_t err;
  CO_EM_t* EM = (CO_EM_t*)CANmodule->em;

  err = CANmodule->CANdrv->can->ESR;

  if(CANmodule->errOld != err) {
    CANmodule->errOld = err;

    /* CAN RX bus overflow */
    if(CANmodule->CANbaseAddress->RF0R & CAN_RF0R_FULL0){
      CO_errorReport(EM, CO_EM_CAN_RXB_OVERFLOW, CO_EMC_CAN_OVERRUN, err);
      CANmodule->CANbaseAddress->RF0R &= ~CAN_RF0R_FULL0; /* clear bits */
    }

    /* CAN TX bus off */
    if(err & CAN_ESR_BOFF) {
      CO_errorReport(EM, CO_EM_CAN_TX_BUS_OFF, CO_EMC_CAN_OVERRUN, err);
    } else {
      CO_errorReset(EM, CO_EM_CAN_TX_BUS_OFF, err);
    }

    /* CAN TX or RX bus passive */
    if(err & CAN_ESR_EPVF){
      if(!CANmodule->firstCANtxMessage){
        CO_errorReport(EM, CO_EM_CAN_TX_BUS_PASSIVE, CO_EMC_CAN_PASSIVE, err);
      }
    } else {
      CO_errorReset(EM, CO_EM_CAN_TX_BUS_PASSIVE, err);
      CO_errorReset(EM, CO_EM_CAN_TX_OVERFLOW, err);
    }


    /* CAN TX or RX bus warning */
    if(err & CAN_ESR_EWGF){
       CO_errorReport(EM, CO_EM_CAN_BUS_WARNING, CO_EMC_NO_ERROR, err);
    }
    else{
       CO_errorReset(EM, CO_EM_CAN_BUS_WARNING, err);
    }
  } /* if(CANmodule->errOld != err) */
}


/**
 * CAN interrupt to receive CAN messages.
 *
 * Function must be called directly from _C1Interrupt or _C2Interrupt with
 * high priority. For more information see topics: <Reception of CAN messages>
 * and <Transmission of CAN messages>.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 *
 * \return TRUE if function completed succesfully
 */
bool_t CO_CANinterrupt_Rx(CO_CANmodule_t *CANmodule)
{
  /* return CO_ERROR? */
  CANRxFrame    can_rx_frame;  /* CAN RX frame to send to ChibiOS CAN HAL API */
  CO_CANrxMsg_t co_rx_msg;     /* CAN RX message to send to CANopenNode API */
  CO_CANrx_t *msg_buff;
  msg_t can_status;
  uint16_t index;
  uint16_t msg;
  uint8_t rtr;
  bool_t msg_matched = FALSE;
  bool_t ret = FALSE;

  can_status = canReceive(CANmodule->CANdrv,
                  0,
                  &can_rx_frame,
                  TIME_IMMEDIATE);

  if(can_status == MSG_TIMEOUT) {
    return ret;
  }

  msg_buff = CANmodule->rxArray;

  msg = can_rx_frame.SID;
  rtr = can_rx_frame.RTR;

  for(index = 0U; index < CANmodule->rxSize; index++) {
    if( ( ((msg ^ msg_buff->ident) & msg_buff->mask) == 0 )
        && ( (rtr ^ msg_buff->rtr) == 0 ) ) {
      msg_matched = TRUE;
      break;
    }

    msg_buff++;
  }

  /* Call specific function, which will process the message */
  if (msg_matched && msg_buff->pFunct) {
    /* TODO: figure out what to do if copying is unsuccessful */
    (void)_co_rx_msg_convert(&co_rx_msg, &can_rx_frame);
    msg_buff->pFunct(msg_buff->object, &co_rx_msg);
  }

  return ret;
}

/**
 * CAN interrupt to transmit CAN messages.
 *
 * Function must be called directly from _C1Interrupt or _C2Interrupt with
 * high priority. For more information see topics: <Reception of CAN messages>
 * and <Transmission of CAN messages>.
 *
 * \param[in] CANmodule - pointer to active CANmodule object
 */
void CO_CANinterrupt_Tx(CO_CANmodule_t *CANmodule)
{
  // CAN_ITConfig(CANmodule->CANbaseAddress, CAN_IT_TME, DISABLE); // Transmit mailbox empty interrupt
  
  /* First CAN message (bootup) was sent successfully */
  CANmodule->firstCANtxMessage = 0;
  /* clear flag from previous message */
  CANmodule->bufferInhibitFlag = 0;

  /* Are there any new messages waiting to be sent and buffer is free? */
  if (CANmodule->CANtxCount > 0) {
    uint16_t index; /* index of transmitting message */

    /* search through whole array of pointers to transmit message buffers. */
    for (index = 0; index < CANmodule->txSize; index++) {
      /* get specific buffer */
      CO_CANtx_t *buffer = &CANmodule->txArray[index];

      /* if message buffer is full, send it */
      if (buffer->bufferFull) {
        int8_t tx_mbx = _get_free_tx_buff(CANmodule);

        if (tx_mbx == -1) {
          // CAN_ITConfig(CANmodule->CANbaseAddress, CAN_IT_TME, ENABLE); /* Transmit mailbox empty interrupt */
          break;
        }

        /* messages with syncFlag set (synchronous PDOs) must be transmited inside preset time window */
        CANmodule->bufferInhibitFlag = buffer->syncFlag;

        /* Copy message to CAN buffer */
        (void)_can_send_to_module(CANmodule, buffer, tx_mbx);

        /* release buffer */
        buffer->bufferFull = 0;
        CANmodule->CANtxCount--;
      } /* if (buffer->bufferFull) */
    } /* for(...) */
  } /* if (CANmodule->CANtxCount > 0) */
}
