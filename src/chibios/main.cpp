/*
    ChibiOS - Copyright (C) 2006..2015 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "ch.hpp"
#include "hal.h"
#include "ch_test.h"

extern "C" {
#include "CANopen.h"
}

#ifdef DRIVER_TEST
#error "DRIVER_TEST in CO_driver.h must be undefined when compiling production code"
#endif

#define TMR_TASK_INTERVAL   (1000)          /* Interval of tmrTask thread in microseconds */
#define INCREMENT_1MS(var)  (var++)         /* Increment 1ms variable in tmrTask */


/* Global variables and objects */
volatile uint16_t   CO_timer1ms = 0U;   /* variable increments each millisecond */


using namespace chibios_rt;

/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  System::init();

  CO_NMT_reset_cmd_t reset = CO_RESET_NOT;

  /* Configure microcontroller. */


  /* initialize EEPROM */


  /* increase variable each startup. Variable is stored in EEPROM. */
  OD_powerOnCounter++;

  while(reset != CO_RESET_APP){
    CO_ReturnError_t err;
    uint16_t timer1msPrevious;

    /* disable CAN and CAN interrupts */


    /* initialize CANopen */
    err = CO_init(0/* CAN module address */, 10/* NodeID */, 125 /* bit rate */);
    if(err != CO_ERROR_NO){
      while(1);
      /* CO_errorReport(CO->em, CO_EM_MEMORY_ALLOCATION_ERROR, CO_EMC_SOFTWARE_INTERNAL, err); */
    }


    /* Configure Timer interrupt function for execution every 1 millisecond */


    /* Configure CAN transmit and receive interrupt */


    /* start CAN */
    CO_CANsetNormalMode(CO->CANmodule[0]);

    reset = CO_RESET_NOT;
    timer1msPrevious = CO_timer1ms;

    while(reset == CO_RESET_NOT){
      uint16_t timer1msCopy, timer1msDiff;

      timer1msCopy = CO_timer1ms;
      timer1msDiff = timer1msCopy - timer1msPrevious;
      timer1msPrevious = timer1msCopy;


      /* CANopen process */
      reset = CO_process(CO, timer1msDiff, NULL);

      /* Nonblocking application code may go here. */

      /* Process EEPROM */
    }
  }


  /* stop threads */


  /* delete objects from memory */
  CO_delete(0/* CAN module address */);


  /* reset */

  return 0;
}
